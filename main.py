from functions import m_json
from functions import m_pck


path = "/calorimetry_home/datasheets/setup_heat_capacity.json"
metadata = m_json.get_metadata_from_setup('/home/pi//calorimetry_home/datasheets/setup_newton.json')

m_pck.check_sensors()

meta_data = m_json.add_temperature_sensor_serials('/calorimetry_home/data', metadata)

"""json_entry = m_json.get_json_entry(folder_path: str, uuid: str, json_path: List[str])"""

meas_data = m_pck.get_meas_data_calorimetry(meta_data)

logged = logging_calorimetry(data, meta_data, '/calorimetry_home/data', path)

archive = m_json.archiv_json('/calorimetry_home', path, '/calorimetry_home/datasheets')

